function s:in_tmux()
    return $TMUX != ""
endfunction

function! tester#find_test()
    let extension = expand('%:e')
    if &filetype == 'perl' && extension == 't'
        return expand('%:p')
    elseif &filetype == 'perl' && extension == 'pm'
        return substitute(expand('%:p'), '/lib/\(.*\)\.pm$', '/t/unit/\1.t', '')
    elseif &filetype == 'ruby' && extension == 'rb'
        return substitute(expand('%:p'), '/lib/\(.*\)/\(.*\.rb\)$', '/test/\1/tc_\2', '')
    elseif &filetype == 'cucumber'
        return substitute(expand('%:p'), '.*/\(features/\)\@=', '', '')
    else
        throw 'Cannot find test'
    end
endfunction

function! s:run_with_tmux_pane(command)
    " Leave results onscreen for 2s on success, 12s on failure
    execute "silent !tmux split-window -d -h -p 30 '" a:command "|| sleep 10 && sleep 2'"
endfunction

function! tester#close_test_pane()
    if s:test_pane != ""
    endif
endfunction

function! tester#run_test()
    let test_commands = {}
    let test_commands['perl']     = 'make test TEST=$test VERBOSE=1'
    let test_commands['ruby']     = 'rake test TEST=$test VERBOSE=1'
    let test_commands['cucumber'] = 'behave $test'

    let test = tester#find_test()

    let test_command = substitute(test_commands[&filetype], '$test', test, '')

    if s:in_tmux()
        execute s:run_with_tmux_pane(test_command)
    else
        execute '!clear; ' test_command
    endif
endfunction

function! tester#run_all_tests()
    let test_commands = {}
    let test_commands['perl']     = 'make test'
    let test_commands['ruby']     = 'rake test'
    let test_commands['cucumber'] = 'behave'

    if s:in_tmux()
        execute s:run_with_tmux_pane(test_commands[&filetype])
    else
        execute '!clear; ' test_commands[&filetype]
    end
endfunction
